import Vue from "vue";
import VueRouter from "vue-router";
import Registration from "./views//Registration.vue";
import PlayGame from "./views//PlayGame.vue";
import EndGame from "./views//EndGame.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Registration",
    component: Registration
  },
  {
    path: "/play-game",
    name: "PlayGame",
    component: PlayGame
  },
  {
    path: "/end-game",
    name: "EndGame",
    component: EndGame
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;