import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    playerName: '',
    isLogList: false,
    isPlayerWin: false,
    isMonsterWin: false,
    logList: [],
  },

  mutations: {
    playerNameChange(state, name) {
      state.playerName = name;
    },

    isLogListChange(state, bool) {
      state.isLogList = bool;
    },

    isPlayerWinChange(state, bool) {
      state.isPlayerWin = bool;
    },

    isMonsterWinChange(state, bool) {
      state.isMonsterWin = bool;
    },

    logListChange(state, array) {
      state.logList = array;
    },
  },

  actions: {
    playerNameChange ({ commit }, name) {
      commit('playerNameChange', name);
    },

    isLogListChange({ commit }, bool) {
      commit('isLogListChange', bool);
    },

    isPlayerWinChange({ commit }, bool) {
      commit('isPlayerWinChange', bool);
    },

    isMonsterWinChange({ commit }, bool) {
      commit('isMonsterWinChange', bool);
    },

    logListChange({ commit }, array) {
      commit('logListChange', array);
    },
  },
});

export default store;